# Socketflow

Socketflow es un servidor que usa express y socket.io de tipo RESTFul API para la optimización y automatización de entrega de datos en tiempo real, construido para el uso exclisivo de la empresa FireCodes y asociados.

  - Entrega de datos y eventos en tiempo real.
  - Backend para conexiones a bases de datos externas.
  - Servicio para FCM.

# Nuevas Características!

  - Primera versión del proyecto

### Tecnologías utilizadas

Dillinger uses a number of open source projects to work properly:

* [Pug] - Para manejar la estructura de las vistas!
* [node.js] - eventos I/O para el backend
* [Express] - Framework node.js para construir aplicaciones de tipo API rapidamente
* [Socket.io] - Para servir los datos y eventos en tiempo real
* [PostgreSQL] - Base de datos.
* [Gulp] - El sistema de streaming build

### Instalación

Socketflow requiere [Node.js](https://nodejs.org/) v4+ para ejecutarse.

Instala las dependencias y ejecuta el servidor.

```sh
$ cd socketflow
$ npm install -S
$ npm run start
```

### Powered by [FireCodes] and partners.


   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>
   [pug]: https://pugjs.org/api/getting-started.html
   [Socket.io]: https://socket.io/
   [FireCodes]: https://www.facebook.com/firecodesapp
   [PostgreSQL]: https://www.postgresql.org/
