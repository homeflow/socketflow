import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import {Request, Response} from "express";
import {Routes} from "./routes";
/// <reference lib="es2017.string" />
import axios from 'axios';
import * as admin from "firebase-admin";
import {Encuesta} from "./entity/Encuesta";
import {Seccion} from "./entity/Seccion";
import {Pregunta} from "./entity/Pregunta";
import {CondicionPregunta} from "./entity/CondicionPregunta";
import path = require('path');
import logger = require('morgan');
import createError = require('http-errors');
import http = require('http');
import socketIO = require('socket.io');
import debug = require('debug');
import Timestamp = admin.firestore.Timestamp;

createConnection().then(async connection => {

    const firebaseAdmin = admin.initializeApp({
        credential: admin.credential.cert(JSON.parse(Buffer.from(process.env.FIREBASECONFIG, 'base64').toString('ascii'))),
        databaseURL: "https://surveyflowappl.firebaseio.com/"
    })

    console.log("La instancia de la conexion a firebase es: ", firebaseAdmin.name);

    /**
     * Functiont that normalize the port given
     */
    function normalizePort(val) {
        const port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    // Create custom listening port
    const port = normalizePort(process.env.PORT || '3000');

    // create express app
    const app = express();
    app.set('port', port);

    // View Engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'pug');
    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));
    app.use(express.static(path.join(__dirname, '../public')));

    // register express routes from defined application routes
    Routes.forEach(typeRoute => {
        typeRoute.forEach(route => {
            (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then((result) => {
                        if (result === null || result === undefined) {
                            res.status(404);
                            return res.send({
                                status: 404,
                                response: "Element not found",
                                data: {}
                            });
                        } else {
                            return res.send({
                                status: 200,
                                response: "Elements reached!.",
                                data: result
                            });
                        }
                    });

                } else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            });
        });
    });

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        next(createError(404));
    });

    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });

    let server = http.createServer(app)
    let io = socketIO(server);

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        const bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Function that is going to ping every 5 minutes to heroku to keep it running
     */
    function keepAliveHeroku() {
        const url = port === 3000 ? 'http://localhost:3000/ping' : 'https://socketflow.herokuapp.com/ping';
        axios.get(url).then(response => {

        }).catch(error => {
            console.log("Error making GET recursive ping to heroku to keep it alive: ", error.toString());
        });
    }

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        const addr = server.address();
        const bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        console.log('Listening on ' + bind);
        debug("Listening on " + bind);

        // Call the keekAliveHeroku function each 5 minutes
        keepAliveHeroku();
        setInterval(keepAliveHeroku, 60000 * 5);
    }

    // setup express app here
    // ...

    // start express server
    server.listen(port);
    server.on('listening', onListening);
    server.on('error', onError);


    module.exports = io;

    /*console.log("El firebaseConfig en base64 es: ",
        Buffer.from(JSON.stringify({
            "type": "service_account",
            "project_id": "surveyflowappl",
            "private_key_id": "a390ddd580444a31bcd549a5d9038fd37ed0b384",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCR9zIb8iS9AiBj\ne/4jFvFZZqlnYJwWnr5Dc+7OnkfKWhsG7QDMf1sMUOTcwWYMzXu+NGSM8l9cg7m3\nVm18q2ps8iIhsqtxklWMSwaVFilgbVUTqrXAz757SbvMVdV2QrVUQs5gmoH4rHcS\nvBbA0Mg/Iy22j0705zzNn3YQ2qO5OI5htGWJMeUAhm63BLsDbFkIAbSEjNRmSVyp\nEPLT8QbWrKixQhFcJU2MyfpU51t/v713CgNjxWU9VPRdK1d+j6In6TVSaHU85E6d\n7VYv2LPiso3lWMP1sNObk/QsFWYbTe2bJBuZfR9kB2o+FkgqPybrYU1sCBn3ZJ8m\nsKLp5CkTAgMBAAECggEACs6/TpNoxffk1wjkwctelmyECYv9Mq2RT+RDH9hXX3O5\ngZNF1l5tquVIdNH7EOKcEXLGqSr/BZYZR6pmE++LQ0LQZSdaC2A25lrdw2cvLB0c\nSgb/SyX2qCFG/vOyCP4aeAE0T7HRAs0eSexUUPWEhixAI77LrVV7yovOjaPqsHmT\nbC+096ldohWcPkoyS80RgC60IXkPYAih82HHI+wEih8nSywurEJsjscBM2Zhrg8T\nHT/Q2pRO9vxQzzEhAkEo5hjZTouKFL0Q/HYh6Ns302KyjUppe7HYzMAvwjCBJuT/\nrxjPgluNGHq9rf0cEcEKP0f2SlEsvLQ5NWHg11ARlQKBgQDEPotmqSSL87yNEfOL\nYo/GhsZvzBEZF1qZMDarenplwbAHQZfjxndP7hogOkDrSH+c4qZf6iO99re3Xv1S\ndnuNuJECS9xjB3ZhrtqscsHKmEvZHiOWY8wniov1DXYuKwGZKlrOEsdx1Xyzi8s5\n/FbMZozPRPGqmUPglZMGck2CXwKBgQC+aV84kjgQnkBUFBiRQQlztVFN1pIbve55\nZsWElsjF2ISWjRqi5DPKVIIdB8/SSv6KwtGGR7Blf+SMmg0x0+uAi6Kd/p2NnhBS\n784jr3i8/wme9su65f7rLy6j25TH9/4dTiAv14d3RNmTcXC/KFYIrUh5SMz+0YjP\nkz9qjLIdzQKBgDLtyF+sJb/sUVKLBhl2SrnK4FTDMOzCEtEOoiF+d6VQzs4TR8fl\nCbHEqSMT+yUUe4nLiEH8EaqLDMKZ9EQiW448B+YAqgFJPn+7USTP41sgiw6ds76j\nx6e7XPcLt0/j+VY2l7pDRV9VeDdgYC18P0pskr53tgAAJo1yQ+W4ZEUhAoGBAKU0\nTU6K5qPcedm5Gduk/utqbKFJQrlZH/gq58JzBL1LbzYkNPERZ7Hrgnxnk7NUJ+cC\nwsFrn6tgvE4VNs99m72K6UuFZvNBKc6bi1HA49Ouv40xeW+uL2+kHyZB1KNvEgYh\nC6IWsM0VamUXrofcGlEq709zj5CjAb1/dy0d50gdAoGARmyR4XpxGGNEOkDUpZMq\n20H8uegxWH9waqx6yITVn8oYOheCmfpwQfZDBzFfqe4sAsu7B4Fq+/ceQX0jiIb5\nl5sAZY/4+qlB8A3uMYYVZ/WW6ebE3tbFc7Jq4ind1zMVJvuvLwyHZ7OBsvvcJMEE\nx7C/5wvOuXmV3QkOLKa9dAQ=\n-----END PRIVATE KEY-----\n",
            "client_email": "firebase-adminsdk-uyhig@surveyflowappl.iam.gserviceaccount.com",
            "client_id": "114881952564755894609",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-uyhig%40surveyflowappl.iam.gserviceaccount.com"
        })).toString('base64'),
        " <- aqui termina el firebaseConfig base64"
    );*/

    /**
     * Codigo para insertar todas las encuestas ya existentes a la nueva BD
     */
    firebaseAdmin.firestore().collection("Encuestas").get().then(async (encuestas) => {
        for (const encuestaFS of encuestas.docs) {
            let encuestaPS = encuestaFS.data();
            let encuesta = new Encuesta();
            let secciones: Seccion[] = [];
            let cantidadPreguntas: number = 0;
            let encuestasRepository = connection.getRepository(Encuesta);

            encuesta.IDString = encuestaPS["ID"];
            encuesta.IDString = encuesta.IDString.replace(/[^A-Za-z0-9]/g, '');
            encuesta.IDString = "SURV-" + encuesta.IDString.substr(0, 7);

            encuesta.OrganizacionPerteneciente = encuestaPS["OrganizacionPerteneciente"];
            encuesta.Asignada = encuestaPS["Asignada"];
            encuesta.CantidadEncuestados = encuestaPS["Encuestados"];
            (encuestaPS["Secciones"] as []).forEach((seccion) => {
                cantidadPreguntas += (seccion["Preguntas"] as []).length;
            });
            encuesta.CantidadPreguntas = cantidadPreguntas;
            encuesta.CantidadSecciones = encuestaPS["Secciones"].length;
            encuesta.Descripcion = encuestaPS["Descripcion"];
            encuesta.FechaCreacion = (encuestaPS["FechaCreacion"] as Timestamp).toDate();
            encuesta.Nombre = encuestaPS["Nombre"];

            (encuestaPS["Secciones"] as []).forEach((seccionFS) => {
                let seccion = new Seccion();
                let preguntas: Pregunta[] = [];

                seccion.IDString = seccionFS["ID"];
                seccion.IDString = seccion.IDString.replace(/[^A-Za-z0-9]/g, '');
                seccion.IDString = "SECT-" + seccion.IDString.substr(0, 7);

                seccion.CantidadPreguntas = (seccionFS["Preguntas"] as []).length;
                seccion.Descripcion = seccionFS["Descripcion"];
                seccion.Nombre = seccionFS["Nombre"];

                (seccionFS["Preguntas"] as []).forEach((preguntaFS) => {
                    let pregunta = new Pregunta();
                    let condiciones: CondicionPregunta[] = [];

                    pregunta.IDString = preguntaFS["ID"];
                    pregunta.IDString = pregunta.IDString.replace(/[^A-Za-z0-9]/g, '');
                    pregunta.IDString = "QUES-" + pregunta.IDString.substr(0, 7);

                    pregunta.Condicionada = preguntaFS["Condicionada"];
                    pregunta.Configuracion = JSON.stringify(preguntaFS["Valores"]);
                    pregunta.Otros = preguntaFS["Otros"];
                    pregunta.Pregunta = preguntaFS["Pregunta"];
                    pregunta.Requerida = preguntaFS["Requerida"];
                    pregunta.Respuesta = JSON.stringify(preguntaFS["Respuesta"]);
                    pregunta.Tipo = preguntaFS["Tipo"];

                    if (pregunta.Condicionada) {
                        let condicion = new CondicionPregunta();
                        condicion.generarIdCondicion();
                        condicion.Condicion = preguntaFS["Condicion"]["Condicion"];
                        condicion.IDPreguntaCondicion = preguntaFS["Condicion"]["IDPreguntaCondicion"] === undefined ? preguntaFS["Condicion"]["IndexPreguntaCondicion"] : preguntaFS["Condicion"]["IDPreguntaCondicion"];
                        condicion.IDSeccionCondicion = preguntaFS["Condicion"]["IDSeccionCondicion"] === undefined ? preguntaFS["Condicion"]["IndexSeccionCondicion"] : preguntaFS["Condicion"]["IDSeccionCondicion"];
                        condicion.OperadorLogico = preguntaFS["Condicion"]["OperadorLogico"];
                        condiciones.push(condicion);
                    }

                    pregunta.Condiciones = condiciones;
                    preguntas.push(pregunta);
                });

                seccion.Preguntas = preguntas;
                secciones.push(seccion);
            });

            encuesta.Secciones = secciones;

            await encuestasRepository.save(encuesta);

        }
    });

    // insert new users for test
    /*await connection.manager.save(connection.manager.create(User, {
        firstName: "Admin",
        lastName: "Admin",
        age: 21,
        role: "ADMIN"
    }));*/

}).catch(error => console.log(error));
