import {EncuestaController} from "../controller/EncuestaController";

export const encuestasRoutes = [
    {
        method: "get",
        route: "/encuestas/",
        controller: EncuestaController,
        action: "all"
    },
    {
        method: "get",
        route: "/encuestas/organizacion/:organizacionPerteneciente",
        controller: EncuestaController,
        action: "allByOrganization"
    },
    {
        method: "get",
        route: "/encuestas/:id",
        controller: EncuestaController,
        action: "one"
    },
    {
        method: "post",
        route: "/encuestas",
        controller: EncuestaController,
        action: "save"
    },
    {
        method: "delete",
        route: "/encuestas/:id",
        controller: EncuestaController,
        action: "remove"
    }
];