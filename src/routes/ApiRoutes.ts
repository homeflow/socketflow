import {ApiController} from "../controller/ApiController";

export const apiRoutes = [
    {
        method: "get",
        route: "/ping",
        controller: ApiController,
        action: "ping"
    }
];