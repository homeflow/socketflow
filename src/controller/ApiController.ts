import {NextFunction, Request, Response} from "express";

export class ApiController {
    async ping(request: Request, response: Response, next: NextFunction) {
        return "¡Keeping alive heroku server!.";
    }
}