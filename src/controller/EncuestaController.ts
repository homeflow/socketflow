import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Encuesta} from "../entity/Encuesta";

export class EncuestaController {

    private encuestaRepository = getRepository(Encuesta);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.encuestaRepository.find({relations: ["Secciones", "Secciones.Preguntas", "Secciones.Preguntas.Condiciones"]});
    }

    async allByOrganization(request: Request, response: Response, next: NextFunction) {
        return this.encuestaRepository.find({
            where: {
                OrganizacionPerteneciente: request.params.organizacionPerteneciente
            },
            relations: ["Secciones", "Secciones.Preguntas", "Secciones.Preguntas.Condiciones"]
        });
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.encuestaRepository.findOne(request.params.organizacionPerteneciente, {relations: ["Secciones", "Secciones.Preguntas", "Secciones.Preguntas.Condiciones"]});
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.encuestaRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.encuestaRepository.findOne(request.params.id);
        await this.encuestaRepository.remove(userToRemove);
    }

}