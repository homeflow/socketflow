import {userRoutes} from "./routes/UserRoutes";
import {encuestasRoutes} from "./routes/EncuestaRoutes";
import {apiRoutes} from "./routes/ApiRoutes";

export const Routes = [
    userRoutes,
    encuestasRoutes,
    apiRoutes
];