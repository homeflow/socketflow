import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    Generated,
    ManyToOne,
    OneToMany,
    Unique,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty, Length} from "class-validator";
import {Encuesta} from "./Encuesta";
import {Pregunta} from "./Pregunta";


@Entity({
    name: "Secciones"
})
@Unique(["IDNumber", "IDString"])
export class Seccion extends BaseEntity {
    @Column({
        type: "bigint",
        name: "IDNumber",
        readonly: true
    })
    @Generated("increment")
    @IsNotEmpty()
    IDNumber: number;

    @Column({
        name: "IDString",
        readonly: true,
        primary: true,
        length: 50,
        nullable: false
    })
    @Length(12, 50)
    @IsNotEmpty()
    IDString: string;

    @Column({
        name: "Nombre",
        length: 100,
        nullable: false
    })
    @IsNotEmpty()
    @Length(1, 100)
    Nombre: string;

    @Column({
        name: "Descripcion",
        length: 500,
        nullable: true
    })
    @Length(0, 500)
    Descripcion: string;

    @Column({
        name: "CantidadPreguntas",
        nullable: false,
    })
    @IsNotEmpty()
    CantidadPreguntas: number;

    @Column({
        name: "CreatedAt"
    })
    @CreateDateColumn()
    createdAt: Date;

    @Column({
        name: "UpdatedAt"
    })
    @UpdateDateColumn()
    updatedAt: Date;

    /**
     * Relacion de tablas para especificar que una [Seccion] puede pertenecier unicamente a una [Encuesta]
     */
    @ManyToOne(type => Encuesta, Encuesta => Encuesta.Secciones)
    Encuesta: Encuesta;

    /**
     * Relacion de tablas para especificar que una [Seccion] puede tener muchas [Pregunta]
     */
    @OneToMany(type => Pregunta, Pregunta => Pregunta.Seccion, {
        cascade: true
    })
    Preguntas: Pregunta[];

    /**
     * Metodo para generar un ID unico de tipo [string] para cada [Seccion] de cada [Encuesta].
     */
    generarIdSeccion(): string {
        let id = 'SECT-';
        const caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 7; i++) {
            id += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
        }
        this.IDString = id;
        return id;
    }
}