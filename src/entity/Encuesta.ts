import {BaseEntity, Column, CreateDateColumn, Entity, Generated, OneToMany, Unique, UpdateDateColumn} from "typeorm";
import {IsNotEmpty, Length} from "class-validator";
import {Seccion} from "./Seccion";

@Entity("Encuestas")
@Unique(["IDNumber", "IDString"])
export class Encuesta extends BaseEntity {

    @Column({
        type: "bigint",
        name: "IDNumber",
        readonly: true
    })
    @Generated("increment")
    IDNumber: number;

    @Column({
        length: 50,
        name: "IDString",
        readonly: true,
        primary: true,
        nullable: false
    })
    @Length(12, 50)
    @IsNotEmpty()
    IDString: string;

    @Column({
        name: "Nombre",
        length: 100,
        nullable: false
    })
    @Length(1, 100)
    @IsNotEmpty()
    Nombre: string;

    @Column({
        name: "Descripcion",
        nullable: true
    })
    @Length(0, 500)
    Descripcion: string;

    @Column({
        name: "OrganizacionPerteneciente",
        nullable: false
    })
    @IsNotEmpty()
    @Length(1, 50)
    OrganizacionPerteneciente: string;

    @Column({
        name: "CantidadSecciones",
        nullable: false
    })
    @IsNotEmpty()
    CantidadSecciones: number;

    @Column({
        name: "CantidadPreguntas",
        nullable: false
    })
    @IsNotEmpty()
    CantidadPreguntas: number;

    @Column({
        name: "CantidadEncuestados",
        nullable: false
    })
    @IsNotEmpty()
    CantidadEncuestados: number;

    @Column({
        name: "Asignada",
        nullable: false,
        array: true,
        type: "text"
    })
    Asignada: string[];

    @Column({
        name: "FechaCreacion",
        nullable: false,
    })
    @IsNotEmpty()
    FechaCreacion: Date;

    @Column({
        name: "CreatedAt"
    })
    @CreateDateColumn()
    createdAt: Date;

    @Column({
        name: "UpdatedAt"
    })
    @UpdateDateColumn()
    updatedAt: Date;

    /**
     * Relacion de tablas para especificar que una [Encuesta] puede tener muchas [Seccion]
     */
    @OneToMany(type => Seccion, Seccion => Seccion.Encuesta, {
        cascade: true
    })
    Secciones: Seccion[];

    /**
     * Metodo para generar un ID unico de tipo [string] para cada Encuesta.
     */
    generarIdEncuesta(): string {
        let id = 'SURV-';
        const caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 7; i++) {
            id += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
        }
        this.IDString = id;
        return id;
    }
}