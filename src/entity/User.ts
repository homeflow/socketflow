import {Entity, PrimaryGeneratedColumn, Column, Unique} from "typeorm";

@Entity('Usuarios')
@Unique(["id"])
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    age: number;

    @Column()
    role: string;

}
