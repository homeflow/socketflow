import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    Generated,
    ManyToOne,
    OneToMany,
    Unique,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty, Length} from "class-validator";
import {Seccion} from "./Seccion";
import {Encuesta} from "./Encuesta";
import {CondicionPregunta} from "./CondicionPregunta";


@Entity({
    name: "Preguntas"
})
@Unique(["IDNumber", "IDString"])
export class Pregunta extends BaseEntity {

    @Column({
        name: "IDNumber",
        type: "bigint",
        readonly: true
    })
    @Generated("increment")
    IDNumber: number;

    @Column({
        name: "IDString",
        length: 50,
        readonly: true,
        primary: true,
        nullable: false
    })
    @Length(12, 50)
    @IsNotEmpty()
    IDString: string;

    @Column({
        name: "Pregunta",
        nullable: false
    })
    @IsNotEmpty()
    Pregunta: string;

    @Column({
        name: "Condicionada",
        nullable: false
    })
    @IsNotEmpty()
    Condicionada: boolean;

    @Column({
        name: "Otros",
        nullable: false
    })
    @IsNotEmpty()
    Otros: boolean;

    @Column({
        name: "Requerida",
        nullable: false
    })
    @IsNotEmpty()
    Requerida: boolean;

    @Column({
        name: "Respuesta",
        nullable: false,
        type: "text"
    })
    Respuesta: string;

    @Column({
        name: "Tipo",
        nullable: false,
    })
    @IsNotEmpty()
    Tipo: string;

    @Column({
        name: "Configuracion",
        nullable: true,
        type: "text"
    })
    Configuracion: string;

    @Column({
        name: "CreatedAt"
    })
    @CreateDateColumn()
    createdAt: Date;

    @Column({
        name: "UpdatedAt"
    })
    @UpdateDateColumn()
    updatedAt: Date;

    /**
     * Relacion de tablas para especificar que una [Pregunta] puede pertenecier unicamente a una [Seccion]
     */
    @ManyToOne(type => Seccion, Seccion => Seccion.Preguntas)
    Seccion: Seccion;

    /**
     * Relacion de tablas para especificar que una [Pregunta] puede tener muchas [Condiciones]
     */
    @OneToMany(type => CondicionPregunta, Condicion => Condicion.Pregunta, {
        cascade: true
    })
    Condiciones: CondicionPregunta[];

    /**
     * Metodo para generar un ID unico de tipo [string] para cada [Pregunta] de cada [Seccion] de cada [Pregunta].
     */
    generarIdPregunta(): string {
        let id = 'QUES-';
        const caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 7; i++) {
            id += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
        }
        this.IDString = id;
        return id;
    }


}