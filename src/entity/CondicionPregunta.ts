import {BaseEntity, Column, CreateDateColumn, Entity, Generated, ManyToOne, Unique, UpdateDateColumn} from "typeorm";
import {IsNotEmpty, Length} from "class-validator";
import {Encuesta} from "./Encuesta";
import {Pregunta} from "./Pregunta";

@Entity({
    name: "CondicionPregunta"
})
@Unique(["IDNumber", "IDString"])
export class CondicionPregunta extends BaseEntity {

    @Column({
        name: "IDNumber",
        type: "bigint",
        readonly: true
    })
    @Generated("increment")
    IDNumber: number;

    @Column({
        name: "IDString",
        length: 50,
        readonly: true,
        primary: true,
        nullable: false
    })
    @Length(12, 50)
    @IsNotEmpty()
    IDString: string;

    @Column({
        name: "Condicion"
    })
    Condicion: string;

    @Column({
        name: "IDPreguntaCondicion"
    })
    IDPreguntaCondicion: string;

    @Column({
        name: "IDSeccionCondicion"
    })
    IDSeccionCondicion: string;

    @Column({
        name: "OperadorLogico"
    })
    OperadorLogico: string;

    @Column({
        name: "CreatedAt"
    })
    @CreateDateColumn()
    createdAt: Date;

    @Column({
        name: "UpdatedAt"
    })
    @UpdateDateColumn()
    updatedAt: Date;

    /**
     * Relacion de tablas para especificar que una [CondicionPregunta] puede pertenecier unicamente a una [Pregunta]
     */
    @ManyToOne(type => Pregunta, Pregunta => Pregunta.Condiciones)
    Pregunta: Pregunta;

    /**
     * Metodo para generar un ID unico de tipo [string] para cada [Condicion] de cada [Pregunta] de cada [Seccion] de cada [Encuesta].
     */
    generarIdCondicion(): string {
        let id = 'COND-';
        const caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 7; i++) {
            id += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
        }
        this.IDString = id;
        return id;
    }
}